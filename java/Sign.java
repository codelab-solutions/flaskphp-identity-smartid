

import ee.sk.smartid.*;
import ee.sk.smartid.rest.*;
import ee.sk.smartid.rest.dao.*;
import org.digidoc4j.*;

import java.security.cert.X509Certificate;
import java.io.*;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.FileUtils;

import java.util.Base64;

import org.json.JSONObject;

public class Sign {

    static String URL = "";
    static String UUID = "";
    static String Name = "";
    static String IDCode = "";
    static String CountryCode = "";
    static String Filename = "";
    static String Environment = "";
    static String SignerInfo = "";
    static String TmpFolder = "";

    public static void main(String[] args) {

        // Args 0 - URL
        // Args 1 - UUID
        // Args 2 - Name
        // Args 3 - ID code
        // Args 4 - country code
        // Args 5 - TEMP file name
        // Args 6 - Environment
        // Args 7 - SignerInfo
        // Args 8 - TmpFolder

        for (int i = 0; i < args.length; i++) {
            switch (i) {
                case 0:
                    setUrl(args[i]);
                    break;
                case 1:
                    setUUID(args[i]);
                    break;
                case 2:
                    setName(args[i]);
                    break;
                case 3:
                    setIDCode(args[i]);
                    break;
                case 4:
                    setCountryCode(args[i]);
                    break;
                case 5:
                    setFilename(args[i]);
                    break;
                case 6:
                    setEnvironment(args[i]);
                    break;
                case 7:
                    setSignerInfo(args[i]);
                    break;
                case 8:
                    setTmpFolder(args[i]);
                    break;
            }
        }

        try {
            PrintStream out = new PrintStream(new FileOutputStream(getFilename()));
            System.setOut(out);
            System.setErr(out);
        } catch (FileNotFoundException ex) {
            System.out.println("[error] " + ex);
        }

        System.out.println("[Starting signing process] ");

        String certPath = "./rp-api_smart-id_com_2024.PEM.crt";

        SmartIdClient client = new SmartIdClient();

        try {
            String content = FileUtils.readFileToString(new File(certPath), "UTF-8");
            client.addTrustedSSLCertificates(content);
        } catch (IOException ex) {
            System.out.println("[error] " + ex);
        }

        client.setRelyingPartyUUID(getUUID());
        client.setRelyingPartyName(getName());
        client.setHostUrl(getUrl());

        NationalIdentity nationalIdentity = new NationalIdentity(getCountryCode(), getIDCode());

        SmartIdCertificate certificateResponse = client
                .getCertificate()
                .withNationalIdentity(nationalIdentity)
                .withCertificateLevel("QUALIFIED")
                .fetch();

        X509Certificate signersCertificate = certificateResponse.getCertificate(); // get the signer's certificate
        String documentNumber = certificateResponse.getDocumentNumber();

        Container container;

        String fileNameWithExt = new File(getFilename()).getName();
        String fileName = FilenameUtils.removeExtension(fileNameWithExt);

        if (getEnvironment().equals("TEST")) {
            Configuration configuration = new Configuration(Configuration.Mode.TEST);
            configuration.setTslLocation("https://open-eid.github.io/test-TL/tl-mp-test-EE.xml");

            //Create a container with a text file to be signed
            container = ContainerBuilder.
                    aContainer("BDOC").
                    withConfiguration(configuration).
                    fromExistingFile(getTmpFolder() + "/" + fileName + ".bdoc").
                    build();
        } else {
            //Create a container with a text file to be signed
            container = ContainerBuilder.
                    aContainer("BDOC").
                    fromExistingFile(getTmpFolder() + "/" + fileName + ".bdoc").
                    build();
        }

        // Decode Signer info
        byte[] decoded = Base64.getDecoder().decode(getSignerInfo());

        String decodedArray = new String(decoded);

        JSONObject jsonObject = new JSONObject(decodedArray);

        SignatureBuilder builder = SignatureBuilder.
                aSignature(container).
                withCity((String) jsonObject.get("city")).
                withStateOrProvince((String) jsonObject.get("state")).
                withPostalCode((String) jsonObject.get("postalcode")).
                withCountry((String) jsonObject.get("country")).
                withRoles((String) jsonObject.get("role")).
                withSigningCertificate(signersCertificate).
                withSignatureDigestAlgorithm(DigestAlgorithm.SHA256);

        //Get the data to be signed by the user
        DataToSign dataToSign = builder.
                buildDataToSign();

        //Data to sign contains the signature dataset with digest of file that should be signed
        byte[] signatureToSign = dataToSign.getDataToSign();

        SignableData dataToSign2 = new SignableData(signatureToSign);
        dataToSign2.setHashType(HashType.SHA256);

        // to display the verification code
        String verificationCode = dataToSign2.calculateVerificationCode();

        System.out.println("[verification_code] " + verificationCode);

        SmartIdSignature smartIdSignature = client
                .createSignature()
                .withDocumentNumber(documentNumber)
                .withSignableData(dataToSign2)
                .withCertificateLevel("QUALIFIED")
                .sign();

        //Sign the signature dataset with digest of file
        byte[] signatureValue = smartIdSignature.getValue();

        //Finalize the signature with OCSP response and timestamp (or timemark)
        Signature signature2 = dataToSign.finalize(signatureValue);

        //Add signature to the container
        container.addSignature(signature2);

        //Save the container as a .bdoc file
        container.saveAsFile(getTmpFolder() + "/" + fileName + ".bdoc");

        System.out.println("[success]");

    }

    public static void setFilename(String filename) {
        Filename = filename;
    }

    public static String getFilename() {
        return Filename;
    }

    public static void setUrl(String url) {
        URL = url;
    }

    public static String getUrl() {
        return URL;
    }

    public static void setUUID(String uuid) {
        UUID = uuid;
    }

    public static String getUUID() {
        return UUID;
    }

    public static void setName(String name) {
        Name = name;
    }

    public static String getName() {
        return Name;
    }

    public static void setIDCode(String idcode) {
        IDCode = idcode;
    }

    public static String getIDCode() {
        return IDCode;
    }

    public static void setCountryCode(String countryCode) {
        CountryCode = countryCode;
    }

    public static String getCountryCode() {
        return CountryCode;
    }

    public static void setEnvironment(String environment) {
        Environment = environment;
    }

    public static String getEnvironment() {
        return Environment;
    }

    public static void setSignerInfo(String signerinfo) {
        SignerInfo = signerinfo;
    }

    public static String getSignerInfo() {
        return SignerInfo;
    }

    public static String getTmpFolder() {
        return TmpFolder;
    }

    public static void setTmpFolder(String tmpFolder) {
        TmpFolder = tmpFolder;
    }
}
