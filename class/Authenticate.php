<?php


	/**
	 *
	 *   FlaskPHP-Identity-SmartID
	 *   --------------------------
	 *   Authentication provider for Smart-ID
	 *
	 *   @author   Codelab Solutions OÜ <codelab@codelab.ee>
	 *   @license  https://www.flaskphp.com/LICENSE MIT
	 *
	 */


	namespace Codelab\FlaskPHP\Identity\SmartID;
	use Codelab\FlaskPHP;
	use RuntimeException;
	use Sk\SmartId\Api\Data\Interaction;
	use Sk\SmartId\Api\Data\SemanticsIdentifier;
	use Sk\SmartId\Client;
	use Sk\SmartId\Api\Data\AuthenticationHash;
	use Sk\SmartId\Api\Data\CertificateLevelCode;
	use Sk\SmartId\Exception\DocumentUnusableException;
	use Sk\SmartId\Exception\InvalidParametersException;
	use Sk\SmartId\Exception\SessionNotFoundException;
	use Sk\SmartId\Exception\SessionTimeoutException;
	use Sk\SmartId\Exception\SmartIdException;
	use Sk\SmartId\Api\AuthenticationResponseValidator;
	use Sk\SmartId\Exception\TechnicalErrorException;
	use Sk\SmartId\Exception\UserAccountNotFoundException;
	use Sk\SmartId\Exception\UserRefusedException;
	use Sk\SmartId\Exception\UserSelectedWrongVerificationCodeException;
	use web_eid\web_eid_authtoken_validation_php\certificate\CertificateData;


	class Authenticate
	{


		/**
		 *   Dev mode?
		 *   @var bool
		 *   @access public
		 */

		public $devMode = false;


		/**
		 *   SmartID UUID
		 *   @var string
		 *   @access public
		 */

		public $smartIDUUID;


		/**
		 *   SmartID name
		 *   @var string
		 *   @access public
		 */

		public $smartIDName;


		/**
		 *   Ask for verification code?
		 *   @var bool
		 *   @access private
		 */

		private $verificationCodeChoice = false;


		/**
		 *  App display text
		 *	@var string
		 *	@access private
		 */

		private $verificationDisplayText = "";


		/**
		 *
		 *   Constructor
		 *   -----------
		 *   @access public
		 *   @param bool $forceDev Force dev environment
		 *   @throws \Exception
		 *   @return FlaskPHP\Identity\SmartID\Authenticate
		 *
		 */

		public function __construct( ?bool $forceDev=null )
		{
			// Add locale
			Flask()->Locale->addLocalePath(__DIR__.'/../locale');

			// Init
			$this->initSmartID($forceDev);
		}


		/**
		 *
		 *   Init the provider
		 *   -----------------
		 *   @access public
		 *   @param bool $forceDev Force dev environment
		 *   @throws \Exception
		 *   @return void
		 *
		 */

		public function initSmartID( ?bool $forceDev=null )
		{
			// Dev mode
			if ($forceDev!==null)
			{
				$this->devMode=$forceDev;
			}
			else
			{
				$this->devMode=Flask()->Debug->devEnvironment;
			}
		}


		/**
		 *
		 *   Init SmartID client
		 *   ----------------
		 *   @access private
		 *   @return Client
		 *
		 */

		private function initSmartidClient()
		{
			// Init dev mode client
			if ($this->devMode)
			{
				$SmartIDClient=new Client();
				$SmartIDClient
					->setRelyingPartyUUID( '00000000-0000-0000-0000-000000000000' )
					->setRelyingPartyName( 'DEMO')
					->setHostUrl( 'https://sid.demo.sk.ee/smart-id-rp/v2/')
					->setPublicSslKeys("sha256//+Tz0G7u3vgcaw/o32vIoCNNjpfo8UugQEXmWkrCuc4o=;sha256//wkdgNtKpKzMtH/zoLkgeScp1Ux4TLm3sUldobVGA/g4=;sha256//Ps1Im3KeB0Q4AlR+/J9KFd/MOznaARdwo4gURPCLaVA=");
			}

			// Init live client
			else
			{
				$SmartIDClient=new Client();
				$SmartIDClient
					->setRelyingPartyUUID( $this->smartIDUUID )
					->setRelyingPartyName( $this->smartIDName )
					->setHostUrl( 'https://rp-api.smart-id.com/v2/')
					->setPublicSslKeys("sha256//5qbYbM98EtA9yIVCQ1HVnkKyqZBUL6kpHoZfuMN+i8o=");
			}

			// Return
			return $SmartIDClient;
		}


		/**
		 *
		 *   Generate SmartID verification code
		 *   ----------------------------------
		 *   @access public
		 *   @param string $idCode Estonian ID code
		 *   @param string|null $countryCode Country code
		 *   @throws UserRefusedException
		 *   @throws SessionTimeoutException
		 *   @throws SessionNotFoundException
		 *   @throws UserAccountNotFoundException
		 *   @throws NotFoundException
		 *   @throws InterruptedException
		 *   @throws DocumentUnusableException
		 *   @throws TechnicalErrorException
		 *   @throws InvalidParametersException
		 *   @throws SmartIdException
		 *   @throws AuthenticateException
		 *   @return string
		 *
		 */

		public function generateVerificationCode( string $idCode, ?string $countryCode='EE' ): string
		{
			try
			{
				// Generate Authentication hash
				$authenticationHash=AuthenticationHash::generate();

				// Start Authentication
				$semanticsIdentifier=SemanticsIdentifier::builder()
					->withSemanticsIdentifierType('PNO')
					->withCountryCode($countryCode)
					->withIdentifier($idCode)
					->build();

				//sleep so in case when user logs in from smart device, he/she can see the verification code on the webpage before the smart id application comes up
				sleep(2);

				$client = $this->initSmartidClient();
				$AllowedInteractionsArray = array();
				if ($this->getAllowVerificationCodeChoice()) $AllowedInteractionsArray[] = Interaction::ofTypeVerificationCodeChoice($this->getVerificationDisplayText());
				$AllowedInteractionsArray[] = Interaction::ofTypeDisplayTextAndPIN($this->getVerificationDisplayText());

				$autheticationInterface=$client->authentication()
					->createAuthentication();
				if (mb_strlen(Flask()->Config->get('identity.smartid.bindto')))
				{
					$autheticationInterface=$autheticationInterface->withNetworkInterface(Flask()->Config->get('identity.smartid.bindto'));
				}
				$sessionId = $autheticationInterface->withSemanticsIdentifier( $semanticsIdentifier ) // or with document number: ->withDocumentNumber( 'PNOEE-10101010005-Z1B2-Q' )
					->withAuthenticationHash( $authenticationHash )
					->withCertificateLevel( CertificateLevelCode::QUALIFIED ) // Certificate level can either be "QUALIFIED" or "ADVANCED"
					->withAllowedInteractionsOrder($AllowedInteractionsArray)
					->startAuthenticationAndReturnSessionId();

				if (!empty($semanticsIdentifier))
				{
					Flask()->Session->set('auth.smartid.sessionid', serialize($sessionId));
				}

				if (!empty($authenticationHash))
				{
					Flask()->Session->set('auth.smartid.hash', serialize($authenticationHash));
				}

				// Generate Verification code from Hash
				return $authenticationHash->calculateVerificationCode();
			}
			catch (UserRefusedException $e)
			{
				throw new UserRefusedException('[[ FLASK.IDENTITY.SmartID.Error.UserRefused ]]'.(($this->devMode && mb_strlen($e->getMessage()))?' -- '.$e->getMessage():''));
			}
			catch (SessionTimeoutException $e)
			{
				throw new SessionTimeoutException('[[ FLASK.IDENTITY.SmartID.Error.SessionTimeout ]]'.(($this->devMode && mb_strlen($e->getMessage()))?' -- '.$e->getMessage():''));
			}
			catch (SessionNotFoundException $e)
			{
				throw new SessionNotFoundException('[[ FLASK.IDENTITY.SmartID.Error.SessionNotFound ]]'.(($this->devMode && mb_strlen($e->getMessage()))?' -- '.$e->getMessage():''));
			}
			catch (UserAccountNotFoundException $e)
			{
				throw new UserAccountNotFoundException('[[ FLASK.IDENTITY.SmartID.Error.UserAccountNotFound ]]'.(($this->devMode && mb_strlen($e->getMessage()))?' -- '.$e->getMessage():''));
			}
			catch (FlaskPHP\Exception\NotFoundException $e)
			{
				throw new FlaskPHP\Exception\NotFoundException('[[ FLASK.IDENTITY.SmartID.Error.NotFound ]]'.(($this->devMode && mb_strlen($e->getMessage()))?' -- '.$e->getMessage():''));
			}
			catch (InterruptedException $e)
			{
				throw new InterruptedException('[[ FLASK.IDENTITY.SmartID.Error.Interrupted ]]'.(($this->devMode && mb_strlen($e->getMessage()))?' -- '.$e->getMessage():''));
			}
			catch (DocumentUnusableException $e)
			{
				throw new DocumentUnusableException('[[ FLASK.IDENTITY.SmartID.Error.DocumentUnusable ]]'.(($this->devMode && mb_strlen($e->getMessage()))?' -- '.$e->getMessage():''));
			}
			catch (TechnicalErrorException $e)
			{
				throw new TechnicalErrorException('[[ FLASK.IDENTITY.SmartID.Error.TechnicalError ]]'.(($this->devMode && mb_strlen($e->getMessage()))?' -- '.$e->getMessage():''));
			}
			catch (InvalidParametersException $e)
			{
				throw new InvalidParametersException(' [[FLASK.IDENTITY.SmartID.Error.InvalidParameters] ]'.(($this->devMode && mb_strlen($e->getMessage()))?' -- '.$e->getMessage():''));
			}
			catch (SmartIdException $e)
			{
				throw new SmartIdException('[[ FLASK.IDENTITY.SmartID.Error.SmartId ]]'.(($this->devMode && mb_strlen($e->getMessage()))?' -- '.$e->getMessage():''));
			}
			catch (\Exception $e)
			{
				throw new AuthenticateException('[[ FLASK.IDENTITY.SmartID.Error.SmartId.Other ]] -- '.$e->getMessage());
			}
		}


		/**
		 *
		 *   Start SmartID auth
		 *   ------------------
		 *   @access public
		 *   @throws UserRefusedException
		 *   @throws SessionTimeoutException
		 *   @throws SessionNotFoundException
		 *   @throws UserAccountNotFoundException
		 *   @throws NotFoundException
		 *   @throws InterruptedException
		 *   @throws DocumentUnusableException
		 *   @throws TechnicalErrorException
		 *   @throws InvalidParametersException
		 *   @throws SmartIdException
		 *   @throws AuthenticateException
		 *   @return AuthenticateResponse
		 *
		 */

		public function startAuth(): AuthenticateResponse
		{
			try
			{
				$client = $this->initSmartidClient();
				$sessionId = unserialize(Flask()->Session->get('auth.smartid.sessionid'));
				$authenticationHash = unserialize(Flask()->Session->get('auth.smartid.hash'));

				$sessionrequest=$client->authentication()->createSessionStatusFetcher();
				if (mb_strlen(Flask()->Config->get('identity.smartid.bindto')))
				{
					$sessionrequest=$sessionrequest->withNetworkInterface(Flask()->Config->get('identity.smartid.bindto'));
				}
				$sessionrequest->withSessionId($sessionId)
					->withAuthenticationHash($authenticationHash)
					->withSessionStatusResponseSocketTimeoutMs(1000);

				$sessionrequest=$sessionrequest->getAuthenticationResponse();
				$sessionStatus = $sessionrequest->getState();


				// Validate response
				if (!empty($sessionStatus))
				{
					switch ($sessionStatus)
					{
						// In progress
						case 'RUNNING':
							$response=new AuthenticateResponse();
							$response->status='pending';
							break;

						// Success
						case 'COMPLETE':
							Flask()->Session->set('auth.smartid.response','');
							Flask()->Session->set('auth.smartid.hash','');

							// create a folder with name "trusted_certificates" and set path to that folder here:
							$pathToFolderWithTrustedCertificates = __DIR__ . '/..';
							$authenticationResponseValidator = new AuthenticationResponseValidator($pathToFolderWithTrustedCertificates);
							$authenticationResult = $authenticationResponseValidator->validate( $sessionrequest );

							// Check if there are any errors
							if ($authenticationResult->isValid()) {
								$response=new AuthenticateResponse();
								$response->status='success';
								$response->firstName=$authenticationResult->getAuthenticationIdentity()->getGivenName();
								$response->lastName=$authenticationResult->getAuthenticationIdentity()->getSurName();
								$response->idCode=$authenticationResult->getAuthenticationIdentity()->getIdentityNumber();
								$response->country=$authenticationResult->getAuthenticationIdentity()->getCountry();
							}
							else {
								throw new RuntimeException("Error! Response is not valid! Error(s): ". implode(",", $authenticationResult->getErrors()));
							}
							break;

						// Error
						default:
							Flask()->Session->set('auth.smartid.response','');
							Flask()->Session->set('auth.smartid.hash','');
							$response=new AuthenticateResponse();
							$response->status='error';
							$response->error='An error has occured';
							break;
					}

					return $response;
				}
				else
				{
					throw new AuthenticateException('[[ FLASK.IDENTITY.SmartID.Error.ServiceError ]]'.(($this->devMode && mb_strlen($e->getMessage()))?' -- '.$e->getMessage():''));
				}
			}
			catch (UserRefusedException $e)
			{
				throw new UserRefusedException('[[ FLASK.IDENTITY.SmartID.Error.UserRefused ]]'.(($this->devMode && mb_strlen($e->getMessage()))?' -- '.$e->getMessage():''));
			}
			catch (SessionTimeoutException $e)
			{
				throw new SessionTimeoutException('[[ FLASK.IDENTITY.SmartID.Error.SessionTimeout ]]'.(($this->devMode && mb_strlen($e->getMessage()))?' -- '.$e->getMessage():''));
			}
			catch (SessionNotFoundException $e)
			{
				throw new SessionNotFoundException('[[ FLASK.IDENTITY.SmartID.Error.SessionNotFound ]]'.(($this->devMode && mb_strlen($e->getMessage()))?' -- '.$e->getMessage():''));
			}
			catch (UserAccountNotFoundException $e)
			{
				throw new UserAccountNotFoundException('[[ FLASK.IDENTITY.SmartID.Error.UserAccountNotFound ]]'.(($this->devMode && mb_strlen($e->getMessage()))?' -- '.$e->getMessage():''));
			}
			catch (FlaskPHP\Exception\NotFoundException $e)
			{
				throw new FlaskPHP\Exception\NotFoundException('[[ FLASK.IDENTITY.SmartID.Error.NotFound ]]'.(($this->devMode && mb_strlen($e->getMessage()))?' -- '.$e->getMessage():''));
			}
			catch (InterruptedException $e)
			{
				throw new InterruptedException('[[ FLASK.IDENTITY.SmartID.Error.Interrupted ]]'.(($this->devMode && mb_strlen($e->getMessage()))?' -- '.$e->getMessage():''));
			}
			catch (DocumentUnusableException $e)
			{
				throw new DocumentUnusableException('[[ FLASK.IDENTITY.SmartID.Error.DocumentUnusable ]]'.(($this->devMode && mb_strlen($e->getMessage()))?' -- '.$e->getMessage():''));
			}
			catch (TechnicalErrorException $e)
			{
				throw new TechnicalErrorException('[[ FLASK.IDENTITY.SmartID.Error.TechnicalError ]]'.(($this->devMode && mb_strlen($e->getMessage()))?' -- '.$e->getMessage():''));
			}
			catch (InvalidParametersException $e)
			{
				throw new InvalidParametersException(' [[FLASK.IDENTITY.SmartID.Error.InvalidParameters] ]'.(($this->devMode && mb_strlen($e->getMessage()))?' -- '.$e->getMessage():''));
			}
			catch (SmartIdException $e)
			{
				throw new SmartIdException('[[ FLASK.IDENTITY.SmartID.Error.SmartId ]]'.(($this->devMode && mb_strlen($e->getMessage()))?' -- '.$e->getMessage():''));
			}
			catch (\Exception $e)
			{
				throw new AuthenticateException('[[ FLASK.IDENTITY.SmartID.Error.SmartId.Other ]] -- '.$e->getMessage());
			}
		}


		/**
		 *
		 *   Set verification code choice
		 *   ----------------------------
		 *   @access public
		 *   @param bool $verificationCodeChoice Verification code choice
		 *   @return void
		 *
		 */

		public function setAllowVerificationCodeChoice( bool $verificationCodeChoice ): void
		{
			$this->verificationCodeChoice=$verificationCodeChoice;
		}


		/**
		 *
		 *   Get verification code choice
		 *   ----------------------------
		 *   @access public
		 *   @return bool
		 *
		 */

		public function getAllowVerificationCodeChoice(): bool
		{
			return $this->verificationCodeChoice;
		}


		/**
		 *
		 *   Set verification display text
		 *   -----------------------------
		 *   @access public
		 *   @param string|null $verificationDisplayText Verification display text
		 *   @return void
		 *
		 */

		public function setVerificationDisplayText( string $verificationDisplayText='' ): void
		{
			$this->verificationDisplayText=$verificationDisplayText;
		}


		/**
		 *
		 *   Get verification display text
		 *   -----------------------------
		 *   @access public
		 *   @return string
		 *
		 */

		public function getVerificationDisplayText(): string
		{
			return $this->verificationDisplayText;
		}

	}


?>
