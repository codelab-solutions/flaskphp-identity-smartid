<?php


	/**
	 *
	 *   FlaskPHP-Identity-SmartID
	 *   --------------------------
	 *   Signing provider for Smart-ID
	 *
	 *   @author   Codelab Solutions OÜ <codelab@codelab.ee>
	 *   @license  https://www.flaskphp.com/LICENSE MIT
	 *
	 */


	namespace Codelab\FlaskPHP\Identity\SmartID;
	use Codelab\FlaskPHP;
	use Codelab\FlaskPHP\Exception\Exception;
	use Sk\SmartId\Exception\CertificateNotFoundException;
	use Sk\SmartId\Exception\ClientNotSupportedException;
	use Sk\SmartId\Exception\DocumentUnusableException;
	use Sk\SmartId\Exception\InterruptedException;
	use Sk\SmartId\Exception\RequestForbiddenException;
	use Sk\SmartId\Exception\ServerMaintenanceException;
	use Sk\SmartId\Exception\SessionNotFoundException;
	use Sk\SmartId\Exception\SessionTimeoutException;
	use Sk\SmartId\Exception\SmartIdException;
	use Sk\SmartId\Exception\TechnicalErrorException;
	use Sk\SmartId\Exception\UnauthorizedException;
	use Sk\SmartId\Exception\UserAccountNotFoundException;
	use Sk\SmartId\Exception\UserRefusedException;


	class Sign
	{


		/**
		 *   Dev mode?
		 *   @var bool
		 *   @access public
		 */

		public $devMode = false;


		/**
		 *   SmartID UUID
		 *   @var string
		 *   @access public
		 */

		public $smartIDUUID;


		/**
		 *   SmartID name
		 *   @var string
		 *   @access public
		 */

		public $smartIDName;



		/**
		 *
		 *   Constructor
		 *   -----------
		 *   @access public
		 *   @param bool $devMode
		 *   @throws SignException
		 *   @return Sign
		 *
		 */

		public function __construct( bool $devMode=null )
		{
			// Add locale
			Flask()->Locale->addLocalePath(__DIR__.'/../locale');

			// Init
			$this->initSmartID($devMode);
		}


		/**
		 *
		 *   Init the provider
		 *   -----------------
		 *   @access public
		 *   @param bool $devMode Force dev environment
		 *   @throws \Exception
		 *   @return void
		 *
		 */

		public function initSmartID( bool $devMode=null )
		{
			// Dev mode
			if ($devMode!==null)
			{
				$this->devMode=$devMode;
			}
			else
			{
				$this->devMode=Flask()->Debug->devEnvironment;
			}
		}


		/**
		 *
		 *   Init SmartID client
		 *   ----------------
		 *   @access public
		 *   @param string $signerIdCode Signer's ID code
		 *   @param string $signerCountry Signer's country
		 * 	 @param string $container
		 *   @param array $signerInfo Additional signer info
		 *   @return Client
		 *
		 */

		public function initSmartIDClient( $idcode, $countryCode, $container, array $signerInfo=null )
		{

			if($this->devMode)
			{
				$url = 'https://sid.demo.sk.ee/smart-id-rp/v1/';
				$UUID = '00000000-0000-0000-0000-000000000000';
				$name = 'DEMO';
				$environment = 'TEST';
			}
			else
			{
				$url = 'https://rp-api.smart-id.com/v1/';
				$UUID = $this->smartIDUUID;
				$name = $this->smartIDName;
				$environment = 'LIVE';
			}

			$uniqueName = uniqid();

			$filename = Flask()->Config->getTmpPath().'/'.$uniqueName.'.txt';

			$BDOCFile=Flask()->Config->getTmpPath().'/'.$uniqueName.'.bdoc';
			file_put_contents($BDOCFile,$container);

			file_put_contents($filename, '');

			Flask()->Session->set('smartid.temp.output.file', $filename);
			Flask()->Session->set('smartid.temp.output.name', $uniqueName);

			// Additional signer info
			$SignerInfo = array();
			$SignerInfo['role'] = (($signerInfo!==null && array_key_exists('role',$signerInfo))?$signerInfo['role']:'');
			$SignerInfo['city'] = (($signerInfo!==null && array_key_exists('city',$signerInfo))?$signerInfo['city']:'');
			$SignerInfo['state'] = (($signerInfo!==null && array_key_exists('state',$signerInfo))?$signerInfo['state']:'');
			$SignerInfo['postalcode'] = (($signerInfo!==null && array_key_exists('postalcode',$signerInfo))?$signerInfo['postalcode']:'');
			$SignerInfo['country'] = (($signerInfo!==null && array_key_exists('country',$signerInfo))?$signerInfo['country']:'');

			$SignerInfo = json_encode($SignerInfo);
			$SignerInfo = base64_encode($SignerInfo);

			$IDCode = $idcode;

			$CountryCode = $countryCode;

			$cmd = 'java -classpath .:./slf4j-api-1.7.21.jar:./commons-lang3-3.9.jar:./jersey-client-2.24.1.jar:./jersey-common-2.24.1.jar:./jaxb-runtime-2.3.1.jar:./jersey-media-json-jackson-2.24.1.jar:./javax.ws.rs-api-2.1.jar:./hk2-api-2.5.0.jar:./hk2-utils-2.5.0.jar:./jersey-apache-connector-2.24.1.jar:./hk2-locator-2.5.0.jar:./javax.inject-1.jar:./jersey-guava-2.24.1.jar:./smart-id-java-client-1.5.jar:./javax.annotation-api-1.3.2.jar:./jackson-annotations-2.8.5.jar:./jackson-jaxrs-json-provider-2.8.5.jar:./jackson-jaxrs-base-2.8.5.jar:./jackson-core-2.8.5.jar:./jersey-entity-filtering-2.24.1.jar:./jackson-databind-2.8.5.jar:./jackson-module-jaxb-annotations-2.8.5.jar:./slf4j-simple-1.7.21.jar:./commons-codec-1.10.jar:./digidoc4j-3.2.0.jar:./snakeyaml-1.24.jar:./commons-io-2.4.jar:./dss-spi-5.4.d4j.1.jar:./dss-model-5.4.d4j.1.jar:./dss-service-5.4.d4j.1.jar:./dss-document-5.4.d4j.1.jar:./dss-xades-5.4.d4j.1.jar:./bcprov-jdk15on-1.62.jar:./xmlsec-2.0.2.jar:./bcpkix-jdk15on-1.62.jar:./httpcore-4.4.11.jar:./httpclient-4.5.9.jar:./commons-logging-1.2.jar:./dss-utils-5.4.d4j.1.jar:./dss-utils-apache-commons-5.4.d4j.1.jar:./commons-collections4-4.4.jar:./dss-common-validation-jaxb-5.4.d4j.1.jar:./dss-tsl-validation-5.4.d4j.1.jar:./dss-tsl-jaxb-5.4.d4j.1.jar:./jaxb-core-2.3.0.1.jar:./validation-policy-5.4.d4j.1.jar:./dss-policy-jaxb-5.4.d4j.1.jar:./dss-reports-5.4.d4j.1.jar:./dss-diagnostic-jaxb-5.4.d4j.1.jar:./dss-detailed-report-jaxb-5.4.d4j.1.jar:./dss-simple-report-jaxb-5.4.d4j.1.jar:./json-20190722.jar Sign '.$url.' '.$UUID.' "'.$name.'" '.$IDCode.' '.$CountryCode.' '.$filename.' '.$environment.' '.$SignerInfo. ' ' . Flask()->Config->getTmpPath() . ' >/dev/null 2>&1 &';

			$errors = '';

			$test = exec_with_cwd($cmd, realpath(__DIR__.'/../java/'), $errors);

			// Return response
			$response=new SignResponse();
			$response->status='OK';
			$response->filename=$filename;
			$response->bdocfilename=$uniqueName;
			return $response;
		}


		/**
		 *
		 *   Start signing session
		 *   ---------------------
		 *   @access public
		 *   @param string $filename filename to read from
		 *   @param string $bdocname Digidoc file name
		 *   @throws SignException
		 *   @return SignResponse
		 *
		 */

		public function getVerificationCode( string $filename, string $bdocname=null )
		{

			try
			{

				$verificationCode='';

				if ($file=fopen($filename, "r"))
				{
					while (!feof($file))
					{
						$line=fgets($file);

						if (strpos($line, '[verification_code]')!==false)
						{
							$verificationCode=preg_replace("/[^0-9]/", '', $line);
							break;
						}
						elseif (strpos($line, '[error]')!==false || strpos($line, 'Exception in thread "main"')!==false)
						{
							$exception=trim(substr($line, strrpos($line, '.')+1));

							// Delete temp file
							unlink($filename);
							unlink(Flask()->Config->getTmpPath().'/'.oneof(Flask()->Session->get('smartid.temp.output.name'), $bdocname).'.bdoc');

							Flask()->Session->set('smartid.temp.output.file', '');
							Flask()->Session->set('smartid.temp.output.name', '');

							switch ($exception)
							{
								case 'CertificateNotFoundException':
									throw new \Codelab\FlaskPHP\Identity\SmartID\CertificateNotFoundException($line);
								case 'ClientNotSupportedException':
									throw new ClientNotSupportedException($line);
								case 'RequestForbiddenException':
									throw new RequestForbiddenException($line);
								case 'ServerMaintenanceException':
									throw new ServerMaintenanceException($line);
								case 'UnauthorizedException':
									throw new UnauthorizedException($line);

								case 'UserRefusedException':
									throw new UserRefusedException($line);
								case 'SessionTimeoutException':
									throw new SessionTimeoutException($line);
								case 'SessionNotFoundException':
									throw new SessionNotFoundException($line);
								case 'UserAccountNotFoundException':
									throw new UserAccountNotFoundException($line);
								case 'InterruptedException':
									throw new InterruptedException($line);
								case 'DocumentUnusableException':
									throw new DocumentUnusableException($line);
								case 'TechnicalErrorException':
									throw new TechnicalErrorException($line);
								case 'InvalidParametersException':
									throw new InvalidParametersException($line);
								case 'SmartIdException':
									throw new SmartIdException($line);
								default:
									throw new SignException($line);
							}

							break;
						}
					}
					fclose($file);
				}

				if (!empty($verificationCode))
				{
					// Return response
					$response=new SignResponse();
					$response->status='pending';
					$response->verificationCode=$verificationCode;
					return $response;
				}
				else
				{
					// Return response
					$response=new SignResponse();
					$response->status='OK';
					return $response;
				}
			}
			catch (UserRefusedException $e)
			{
				throw new SignException('[[FLASK.IDENTITY.SmartID.Error.UserRefused]]');
			}
			catch (SessionTimeoutException $e)
			{
				throw new SignException('[[FLASK.IDENTITY.SmartID.Error.SessionTimeout]]');
			}
			catch (SessionNotFoundException $e)
			{
				throw new SignException('[[FLASK.IDENTITY.SmartID.Error.SessionNotFound]]');
			}
			catch (UserAccountNotFoundException $e)
			{
				throw new SignException('[[FLASK.IDENTITY.SmartID.Error.UserAccountNotFound]]');
			}
			catch (FlaskPHP\Exception\NotFoundException $e)
			{
				throw new SignException('[[FLASK.IDENTITY.SmartID.Error.NotFound]]');
			}
			catch (InterruptedException $e)
			{
				throw new SignException('[[FLASK.IDENTITY.SmartID.Error.Interrupted]]');
			}
			catch (DocumentUnusableException $e)
			{
				throw new SignException('[[FLASK.IDENTITY.SmartID.Error.DocumentUnusable]]');
			}
			catch (TechnicalErrorException $e)
			{
				throw new SignException('[[FLASK.IDENTITY.SmartID.Error.TechnicalError]]');
			}
			catch (InvalidParametersException $e)
			{
				throw new SignException('[[FLASK.IDENTITY.SmartID.Error.InvalidParameters]]');
			}

			catch (\Codelab\FlaskPHP\Identity\SmartID\CertificateNotFoundException $e)
			{
				throw new SignException('[[FLASK.IDENTITY.SmartID.Error.CertificateNotFound]]');
			}
			catch (\Codelab\FlaskPHP\Identity\SmartID\ClientNotSupportedException $e)
			{
				throw new SignException('[[FLASK.IDENTITY.SmartID.Error.ClientNotSupported]]');
			}
			catch (\Codelab\FlaskPHP\Identity\SmartID\RequestForbiddenException $e)
			{
				throw new SignException('[[FLASK.IDENTITY.SmartID.Error.RequestForbidden]]');
			}
			catch (\Codelab\FlaskPHP\Identity\SmartID\ServerMaintenanceException $e)
			{
				throw new SignException('[[FLASK.IDENTITY.SmartID.Error.ServerMaintenance]]');
			}
			catch (\Codelab\FlaskPHP\Identity\SmartID\UnauthorizedException $e)
			{
				throw new SignException('[[FLASK.IDENTITY.SmartID.Error.Unauthorized]]');
			}

			catch (SmartIdException $e)
			{
				throw new SignException('[[FLASK.IDENTITY.SmartID.Error.SmartId]]');
			}
			catch (\Exception $e)
			{
				throw new SignException('[[FLASK.IDENTITY.SmartID.Error.SmartId.Other]]'.$e->getMessage());
			}
		}


		/**
		 *
		 *   Start SmartID auth
		 *   --------------------
		 *   @access public
		 * 	 @param string $filename filename to read from
		 *   @param string $bdocname Digidoc file name
		 *   @throws SignException
		 *   @return SignResponse
		 *
		 */

		public function checkStatus( string $filename, string $bdocname=null )
		{
			try
			{
				$exception = '';

				if ($file = fopen($filename, "r")) {
					while(!feof($file)) {
						$line = fgets($file);

						if(strpos($line, '[success]') !== false)
						{
							// Delete temp file
							unlink($filename);

							$Container = FlaskPHP\DigiDoc\DigiDocContainer::openContainer(Flask()->Config->getTmpPath().'/'.oneof(Flask()->Session->get('smartid.temp.output.name'), $bdocname).'.bdoc');

							$response=new SignResponse();
							$response->status='success';
							$response->signedDocInfo=$Container->getDigiDoc();

							$Container->closeContainer();

							unlink(Flask()->Config->getTmpPath().'/'.oneof(Flask()->Session->get('smartid.temp.output.name'), $bdocname).'.bdoc');

							Flask()->Session->set('smartid.temp.output.file', '');
							Flask()->Session->set('smartid.temp.output.name', '');

							break;
						}
						elseif(strpos($line, '[error]') !== false || strpos($line, 'Exception in thread "main"') !== false)
						{
							$exception = trim(substr($line, strrpos($line, '.') + 1));

							// Delete temp file
							unlink($filename);
							unlink(Flask()->Config->getTmpPath().'/'.oneof(Flask()->Session->get('smartid.temp.output.name'), $bdocname).'.bdoc');

							Flask()->Session->set('smartid.temp.output.file', '');
							Flask()->Session->set('smartid.temp.output.name', '');

							switch ($exception)
							{
								case 'CertificateNotFoundException':
									throw new \Codelab\FlaskPHP\Identity\SmartID\CertificateNotFoundException($line);
								case 'ClientNotSupportedException':
									throw new ClientNotSupportedException($line);
								case 'RequestForbiddenException':
									throw new RequestForbiddenException($line);
								case 'ServerMaintenanceException':
									throw new ServerMaintenanceException($line);
								case 'UnauthorizedException':
									throw new UnauthorizedException($line);

								case 'UserRefusedException':
									throw new UserRefusedException($line);
								case 'SessionTimeoutException':
									throw new SessionTimeoutException($line);
								case 'SessionNotFoundException':
									throw new SessionNotFoundException($line);
								case 'UserAccountNotFoundException':
									throw new UserAccountNotFoundException($line);
								case 'InterruptedException':
									throw new InterruptedException($line);
								case 'DocumentUnusableException':
									throw new DocumentUnusableException($line);
								case 'TechnicalErrorException':
									throw new TechnicalErrorException($line);
								case 'InvalidParametersException':
									throw new InvalidParametersException($line);
								case 'SmartIdException':
									throw new SmartIdException($line);
								default:
									throw new SignException($line);
							}

							break;
						}
						else
						{
							$response=new SignResponse();
							$response->status='pending';
						}
					}
					fclose($file);
				}

				return $response;

			}
			catch (UserRefusedException $e)
			{
				throw new SignException('[[FLASK.IDENTITY.SmartID.Error.UserRefused]]');
			}
			catch (SessionTimeoutException $e)
			{
				throw new SignException('[[FLASK.IDENTITY.SmartID.Error.SessionTimeout]]');
			}
			catch (SessionNotFoundException $e)
			{
				throw new SignException('[[FLASK.IDENTITY.SmartID.Error.SessionNotFound]]');
			}
			catch (UserAccountNotFoundException $e)
			{
				throw new SignException('[[FLASK.IDENTITY.SmartID.Error.UserAccountNotFound]]');
			}
			catch (FlaskPHP\Exception\NotFoundException $e)
			{
				throw new SignException('[[FLASK.IDENTITY.SmartID.Error.NotFound]]');
			}
			catch (InterruptedException $e)
			{
				throw new SignException('[[FLASK.IDENTITY.SmartID.Error.Interrupted]]');
			}
			catch (DocumentUnusableException $e)
			{
				throw new SignException('[[FLASK.IDENTITY.SmartID.Error.DocumentUnusable]]');
			}
			catch (TechnicalErrorException $e)
			{
				throw new SignException('[[FLASK.IDENTITY.SmartID.Error.TechnicalError]]');
			}
			catch (InvalidParametersException $e)
			{
				throw new SignException('[[FLASK.IDENTITY.SmartID.Error.InvalidParameters]]');
			}

			catch (\Codelab\FlaskPHP\Identity\SmartID\CertificateNotFoundException $e)
			{
				throw new SignException('Certificate not found');
			}
			catch (\Codelab\FlaskPHP\Identity\SmartID\ClientNotSupportedException $e)
			{
				throw new SignException('The client-side implementation of this API is old and not supported any more. Relying Party should contact customer support.');
			}
			catch (\Codelab\FlaskPHP\Identity\SmartID\RequestForbiddenException $e)
			{
				throw new SignException('Relying Party has no permission to issue the request. This may happen when Relying Party has no permission to invoke operations on accounts with ADVANCED certificates.');
			}
			catch (\Codelab\FlaskPHP\Identity\SmartID\ServerMaintenanceException $e)
			{
				throw new SignException('Server is under maintenance, retry later.');
			}
			catch (\Codelab\FlaskPHP\Identity\SmartID\UnauthorizedException $e)
			{
				throw new SignException('Unauthorized');
			}

			catch (SmartIdException $e)
			{
				throw new SignException('[[FLASK.IDENTITY.SmartID.Error.SmartId]]');
			}
			catch (\Exception $e)
			{
				throw new SignException('[[FLASK.IDENTITY.SmartID.Error.SmartId.Other]]'.$e->getMessage());
			}
		}

	}
