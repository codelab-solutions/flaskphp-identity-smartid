<?php


	/**
	 *
	 *   FlaskPHP-Identity-SmartID
	 *   --------------------------
	 *   Authentication exception
	 *
	 *   @author   Codelab Solutions OÜ <codelab@codelab.ee>
	 *   @license  https://www.flaskphp.com/LICENSE MIT
	 *
	 */


	namespace Codelab\FlaskPHP\Identity\SmartID;
	use Codelab\FlaskPHP;


	class AuthenticateException extends FlaskPHP\Exception\Exception
	{
	}


?>