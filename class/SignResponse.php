<?php


	/**
	 *
	 *   FlaskPHP-Identity-SmartID
	 *   --------------------------
	 *   Smart ID signing response
	 *
	 *   @author   Codelab Solutions OÜ <codelab@codelab.ee>
	 *   @license  https://www.flaskphp.com/LICENSE MIT
	 *
	 */


	namespace Codelab\FlaskPHP\Identity\SmartID;


	class SignResponse
	{
		

		/**
		 *   Response status
		 *   @var string
		 *     pending  -  request pending
		 *     success  -  successfully authenticated
		 *     error    -  error
		 */

		public $status = null;


		/**
		 *   Signed document info
		 *   @var object
		 */

		public $signedDocInfo = null;


		/**
		 *   Error message
		 *   @var string
		 *   @access public
		 */

		public $error = null;


		/**
		 *   Verification code
		 *   @var string
		 *   @access public
		 */

		public $verificationCode = null;


		/**
		 *
		 *   Get status
		 *   ----------
		 *   @access public
		 *   return string
		 *
		 */

		public function getStatus()
		{
			return $this->status;
		}


		/**
		 *
		 *   Is pending?
		 *   -----------
		 *   @access public
		 *   return bool
		 *
		 */

		public function isPending()
		{
			return ($this->status==='pending'?true:false);
		}


		/**
		 *
		 *   Was a success?
		 *   --------------
		 *   @access public
		 *   return bool
		 *
		 */

		public function isSuccess()
		{
			return ($this->status==='success'?true:false);
		}


		/**
		 *
		 *   Was an error?
		 *   -------------
		 *   @access public
		 *   return bool
		 *
		 */

		public function isError()
		{
			return ($this->status==='error'?true:false);
		}


		/**
		 *
		 *   Get challenge response
		 *   ----------------------
		 *   @access public
		 *   return string
		 *
		 */

		public function getSignature()
		{
			return $this->signedDocInfo;
		}
	}